package com.tagitech.scharzbretttwo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScharzbretttwoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScharzbretttwoApplication.class, args);
	}

}
